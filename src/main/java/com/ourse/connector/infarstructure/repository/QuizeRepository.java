package com.ourse.connector.infarstructure.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ourse.connector.application.Model.Quize;

public interface QuizeRepository extends MongoRepository<Quize, String> {

}
