package com.ourse.connector.infarstructure.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.ourse.connector.application.Model.User;

public interface UserRepository extends MongoRepository<User, String> {
	
	@Query("{ _id : ?0 }")
	public User findByUserId(String userId);

}
