package com.ourse.connector.infarstructure.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.ourse.connector.application.Model.Course;

public interface CourseRepository extends MongoRepository<Course, String> {

	@Query("{ _id : ?0 }")
	public Course findByCourseId(String courseId); 
	
	@Query("{ _id : 0?, unit.courseName : 1? }")
	public Course findByCourseIdAndUnitTitle(String name, String unitName); 
	
}
