package com.ourse.connector.infarstructure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExternalProperties {

	@Value("${recourse.server.token}")
	private String resourceServerToken;

	@Value("${recourse.server.url}")
	private String resourceServerUrl;
	
	@Value("${recourse.server.role}")
	private String resourceServerChangeRoleUrl;
	

	public String getResourceServerChangeRoleUrl() {
		return resourceServerChangeRoleUrl;
	}

	public void setResourceServerChangeRoleUrl(String resourceServerChangeRoleUrl) {
		this.resourceServerChangeRoleUrl = resourceServerChangeRoleUrl;
	}

	@Value("${recourse.server.register}")
	private String resourceServerRegisterUrl;

	public String getResourceServerRegisterUrl() {
		return resourceServerRegisterUrl;
	}

	public void setResourceServerRegisterUrl(String resourceServerRegisterUrl) {
		this.resourceServerRegisterUrl = resourceServerRegisterUrl;
	}

	public String getResourceServerUrl() {
		return resourceServerUrl;
	}

	public void setResourceServerUrl(String resourceServerUrl) {
		this.resourceServerUrl = resourceServerUrl;
	}

	public String getResourceServerToken() {
		return resourceServerToken;
	}

	public void setResourceServerToken(String resourceServerToken) {
		this.resourceServerToken = resourceServerToken;
	}

}
