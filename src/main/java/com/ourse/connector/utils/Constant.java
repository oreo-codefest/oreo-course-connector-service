package com.ourse.connector.utils;

public interface Constant {

	public final String ACTIVE = "ACTIVE";
	public final String INACTIVE = "INACTIVE";
	
	public final String ADMIN = "admin";
	public final String STUDENT = "student";
	public final String MODERATOR = "moderator";
	
	public final String SUCCESS = "SUCCESS";
	public final String FAILD = "FAILD";
	
}
