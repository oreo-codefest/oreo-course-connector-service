package com.ourse.connector.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ourse.connector.application.CourseApplicationService;
import com.ourse.connector.application.Model.Course;

@RestController
public class CourseController {

	@Autowired
	CourseApplicationService courseApplicationService;

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/course", method = RequestMethod.POST)
	public Object createCourse(@RequestBody Course course,
			@RequestHeader(name = "authorization") String authorization) throws JsonProcessingException {

		return courseApplicationService.addCourse(authorization, course);

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/course", method = RequestMethod.PUT)
	public Object updateCourseQuizeId(@RequestBody Course course,
			@RequestHeader(name = "authorization") String authorization) throws JsonProcessingException {

		return courseApplicationService.updateCourse(authorization, course);

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/course", method = RequestMethod.GET)
	public Object getAllCourse(@RequestHeader(name = "authorization") String authorization) throws JsonProcessingException {

		return courseApplicationService.getAllCourse(authorization);

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/course/{id}", method = RequestMethod.GET)
	public Object getCourseById(@PathVariable(name = "id") String courseId,
			@RequestHeader(name = "authorization") String authorization) throws JsonProcessingException {

		return courseApplicationService.getCourseById(courseId, authorization);

	}
	


}
