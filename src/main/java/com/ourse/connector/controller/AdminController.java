package com.ourse.connector.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ourse.connector.application.AdminApplicationService;
import com.ourse.connector.application.DTO.AdminRoleRequestDTO;

@RestController
public class AdminController {

	@Autowired
	AdminApplicationService adminApplicationService;

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/change/role", method = RequestMethod.POST)
	public Object changeRole(@RequestBody AdminRoleRequestDTO adminRoleRequestDTO,
			@RequestHeader(name = "authorization") String authentication) throws JsonProcessingException {
		
		return adminApplicationService.changeRole(adminRoleRequestDTO, authentication);

	}
	


}
