package com.ourse.connector.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ourse.connector.application.CourseApplicationService;
import com.ourse.connector.application.DTO.AssignQuizeRequestDTO;
import com.ourse.connector.application.Model.Quize;

@RestController
public class QuizeController {

	@Autowired
	CourseApplicationService courseApplicationService;

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/course/quiz", method = RequestMethod.POST)
	public Object submitQuize(@RequestBody Quize quize, @RequestHeader(name = "authorization") String authorization)
			throws JsonProcessingException {

		return courseApplicationService.submitQuize(authorization, quize);

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/quiz", method = RequestMethod.POST)
	public Object addQuize(@RequestBody Quize quize, @RequestHeader(name = "authorization") String authorization)
			throws JsonProcessingException {

//		return new String("Hello");
		return courseApplicationService.addQuize(authorization, quize);

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/assignquize", method = RequestMethod.POST)
	public Object assignQuizeToACourse(@RequestBody AssignQuizeRequestDTO quize, @RequestHeader(name = "authorization") String authorization)
			throws JsonProcessingException {

//		return new String("Hello");
		return courseApplicationService.assignQuize(authorization, quize);

	}

}
