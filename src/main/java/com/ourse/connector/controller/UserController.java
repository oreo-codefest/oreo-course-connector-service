package com.ourse.connector.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ourse.connector.application.UserApplicationService;
import com.ourse.connector.application.Model.User;

@RestController
public class UserController {

	@Autowired
	UserApplicationService userApplicationService;

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public Object getAllCourse(@RequestBody User user) throws JsonProcessingException {

		return userApplicationService.createUser(user);

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public Object fetchAllUsers(@RequestHeader(name = "authorization") String authorization)
			throws JsonProcessingException {

//		return new String("nregsfs");
		return userApplicationService.getAllUsers(authorization);

	}

}
