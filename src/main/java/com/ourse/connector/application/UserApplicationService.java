package com.ourse.connector.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ourse.connector.application.DTO.AuthAccessTokenResponseDTO;
import com.ourse.connector.application.DTO.AuthenticationResponseDTO;
import com.ourse.connector.application.Model.User;
import com.ourse.connector.application.transformer.Transformer;
import com.ourse.connector.domain.Auth.AuthIntegrationService;
import com.ourse.connector.domain.User.UserService;
import com.ourse.connector.utils.Constant;

@Service
public class UserApplicationService {

	@Autowired
	AuthIntegrationService authIntegrationService;

	@Autowired
	Transformer transform;

	@Autowired
	UserService userService;

	public Object createUser(User user) throws JsonProcessingException {

		AuthAccessTokenResponseDTO authAccessTokenResponseDTO = authIntegrationService.AuthRegister(user.getEmail(),
				user.getPassword());

		if (authAccessTokenResponseDTO.getAccess_Token() != null) {

			return userService.addUserToDB(user);

		} else {

			return transform.createResponse("USER TOKEN " + Constant.INACTIVE);
		}

	}

	public Object getAllUsers(String authorization) throws JsonProcessingException {
		AuthenticationResponseDTO authenticationResponseDTO = authIntegrationService
				.getAuthenticationResult(authorization);

		if ((boolean) authenticationResponseDTO.isActive()) {

			if (authenticationResponseDTO.getRole().equals(Constant.ADMIN)
					|| authenticationResponseDTO.getRole().equals(Constant.MODERATOR)) {

				return userService.getAllUsers();

			}

			return transform.createResponse("ACCESS DENIED");

		} else {

			return transform.createResponse("USER TOKEN " + Constant.INACTIVE);
		}
	}

}
