package com.ourse.connector.application.transformer;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ourse.connector.application.DTO.CourseResponseDTO;
import com.ourse.connector.application.DTO.ResponseDTO;
import com.ourse.connector.application.DTO.UserResponseDTO;
import com.ourse.connector.application.Model.Course;
import com.ourse.connector.application.Model.User;

@Service
public class Transformer {

	public ResponseDTO createResponse(String message) {

		ResponseDTO ResponseDTO = new ResponseDTO();

		ResponseDTO.setMessage(message);

		return ResponseDTO;

	}

	public CourseResponseDTO toCourseResponseDTO(List<Course> course, String message) {

		CourseResponseDTO courseResponseDTO = new CourseResponseDTO();

		courseResponseDTO.setStatus(message);
		courseResponseDTO.setCourseList(course);

		return courseResponseDTO;

	}
	
	public UserResponseDTO toUserResponseDTO(List<User> user, String message) {

		UserResponseDTO userResponseDTO = new UserResponseDTO();

		userResponseDTO.setStatus(message);
		userResponseDTO.setCourseList(user);

		return userResponseDTO;

	}


}
