package com.ourse.connector.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ourse.connector.application.DTO.AssignQuizeRequestDTO;
import com.ourse.connector.application.DTO.AuthenticationResponseDTO;
import com.ourse.connector.application.DTO.ResponseDTO;
import com.ourse.connector.application.Model.Course;
import com.ourse.connector.application.Model.Quize;
import com.ourse.connector.application.transformer.Transformer;
import com.ourse.connector.domain.Auth.AuthIntegrationService;
import com.ourse.connector.domain.Course.CourseServiceImpl;
import com.ourse.connector.domain.Quize.QuizeImplService;
import com.ourse.connector.utils.Constant;

@Service
public class CourseApplicationService {

	@Autowired
	AuthIntegrationService authIntegrationService;

	@Autowired
	Transformer transform;

	@Autowired
	CourseServiceImpl courseConnectorImplService;

	@Autowired
	QuizeImplService quizeImplService;

	/**
	 * 
	 * @param authorization
	 * @param user
	 * @return
	 * @throws JsonProcessingException
	 */
	public ResponseDTO addCourse(String authorization, Course course) throws JsonProcessingException {

		AuthenticationResponseDTO authenticationResponseDTO = authIntegrationService
				.getAuthenticationResult(authorization);

		if ((boolean) authenticationResponseDTO.isActive()) {

			if (authenticationResponseDTO.getRole().equals(Constant.ADMIN)
					|| authenticationResponseDTO.getRole().equals(Constant.MODERATOR)) {

				return courseConnectorImplService.addCourseToDB(course);

			}

			return transform.createResponse("ACCESS DENIED");

		} else {

			return transform.createResponse("USER TOKEN " + Constant.INACTIVE);
		}

	}

	/**
	 * 
	 * @param authorization
	 * @param quize
	 * @return
	 * @throws JsonProcessingException
	 */
	public ResponseDTO addQuize(String authorization, Quize quize) throws JsonProcessingException {

		AuthenticationResponseDTO authenticationResponseDTO = authIntegrationService
				.getAuthenticationResult(authorization);

		if ((boolean) authenticationResponseDTO.isActive()) {

			if (authenticationResponseDTO.getRole().equals(Constant.ADMIN)
					|| authenticationResponseDTO.getRole().equals(Constant.MODERATOR)) {

				return courseConnectorImplService.addQuizeToDB(quize);

			}

			return transform.createResponse("ACCESS DENIED");

		} else {

			return transform.createResponse("USER TOKEN " + Constant.INACTIVE);
		}

	}

	public ResponseDTO updateCourse(String authorization, Course course) throws JsonProcessingException {

		AuthenticationResponseDTO authenticationResponseDTO = authIntegrationService
				.getAuthenticationResult(authorization);

		if ((boolean) authenticationResponseDTO.isActive()) {

			if (authenticationResponseDTO.getRole().equals(Constant.ADMIN)
					|| authenticationResponseDTO.getRole().equals(Constant.MODERATOR)) {

				return courseConnectorImplService.updateInTheCourse(course);

			}

			return transform.createResponse("ACCESS DENIED");

		} else {

			return transform.createResponse("USER TOKEN " + Constant.INACTIVE);
		}

	}

	public Object getAllCourse(String authorization) throws JsonProcessingException {

		AuthenticationResponseDTO authenticationResponseDTO = authIntegrationService
				.getAuthenticationResult(authorization);

		if ((boolean) authenticationResponseDTO.isActive()) {

			return courseConnectorImplService.getAllCourse();

		} else {

			return transform.createResponse("USER TOKEN " + Constant.INACTIVE);
		}

	}

	public Object getCourseById(String courseId, String authorization) throws JsonProcessingException {

		AuthenticationResponseDTO authenticationResponseDTO = authIntegrationService
				.getAuthenticationResult(authorization);

		if ((boolean) authenticationResponseDTO.isActive()) {

			return courseConnectorImplService.getCourseById(courseId);

		} else {

			return transform.createResponse("USER TOKEN " + Constant.INACTIVE);
		}
	}

	public Object submitQuize(String authorization, Quize quize) {

		return null;
	}

	public Object assignQuize(String authorization, AssignQuizeRequestDTO quize) throws JsonProcessingException {

		AuthenticationResponseDTO authenticationResponseDTO = authIntegrationService
				.getAuthenticationResult(authorization);

		if ((boolean) authenticationResponseDTO.isActive()) {

			if (authenticationResponseDTO.getRole().equals(Constant.ADMIN)
					|| authenticationResponseDTO.getRole().equals(Constant.MODERATOR)) {

				return quizeImplService.addQuizeToDB(quize);

			}

			return transform.createResponse("ACCESS DENIED");

		} else {

			return transform.createResponse("USER TOKEN " + Constant.INACTIVE);
		}
	}

}
