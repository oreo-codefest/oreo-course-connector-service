package com.ourse.connector.application.Model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "courses")
public class Course {

	private String _id;
	private String courseName;
	private String courseType;
	private String courseContent;
	private Quize courseQuiz;
	private List<Course> unit;
	private String courseDescription;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public List<Course> getUnit() {
		return unit;
	}

	public void setUnit(List<Course> unit) {
		this.unit = unit;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseType() {
		return courseType;
	}

	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}

	public String getCourseContent() {
		return courseContent;
	}

	public void setCourseContent(String courseContent) {
		this.courseContent = courseContent;
	}

	public Quize getCourseQuiz() {
		return courseQuiz;
	}

	public void setCourseQuiz(Quize courseQuiz) {
		this.courseQuiz = courseQuiz;
	}

	public String getCourseDescription() {
		return courseDescription;
	}

	public void setCourseDescription(String courseDescription) {
		this.courseDescription = courseDescription;
	}

}
