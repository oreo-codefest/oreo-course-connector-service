package com.ourse.connector.application.Model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.ourse.connector.application.DTO.QuizeAnswerDTO;

@Document(collection = "question")
public class Quize {

	private String id;
	private List<QuizeAnswerDTO> answers;

	public String getQuestionId() {
		return id;
	}

	public void setQuestionId(String questionId) {
		this.id = questionId;
	}

	public List<QuizeAnswerDTO> getAnswers() {
		return answers;
	}

	public void setAnswers(List<QuizeAnswerDTO> answers) {
		this.answers = answers;
	}

}
