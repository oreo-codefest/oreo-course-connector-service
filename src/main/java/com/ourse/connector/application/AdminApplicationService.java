package com.ourse.connector.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ourse.connector.application.DTO.AdminRoleRequestDTO;
import com.ourse.connector.application.DTO.AuthenticationResponseDTO;
import com.ourse.connector.application.transformer.Transformer;
import com.ourse.connector.domain.Admin.AdminServiceImpl;
import com.ourse.connector.domain.Auth.AuthIntegrationService;
import com.ourse.connector.utils.Constant;

@Service
public class AdminApplicationService {

	@Autowired
	AuthIntegrationService authIntegrationService;

	@Autowired
	AdminServiceImpl adminServiceImpl;

	@Autowired
	Transformer transform;

	public Object changeRole(AdminRoleRequestDTO adminRoleRequestDTO, String authorization)
			throws JsonProcessingException {

		AuthenticationResponseDTO authenticationResponseDTO = authIntegrationService
				.getAuthenticationResult(authorization);

		if ((boolean) authenticationResponseDTO.isActive()) {

			if (adminRoleRequestDTO.getRoles().equals(Constant.ADMIN)) {

				return adminServiceImpl.chengeUserRoleAuthServer(adminRoleRequestDTO);

			}

			return transform.createResponse("ACCESS DENIED");

		} else {

			return transform.createResponse("USER TOKEN " + Constant.INACTIVE);
		}

	}

}
