package com.ourse.connector.application.DTO;

import java.util.List;

import com.ourse.connector.application.Model.User;

public class UserResponseDTO {
	
	private String status;
	private List<User> courseList;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<User> getCourseList() {
		return courseList;
	}

	public void setCourseList(List<User> courseList) {
		this.courseList = courseList;
	}
}
