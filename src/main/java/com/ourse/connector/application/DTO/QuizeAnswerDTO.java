package com.ourse.connector.application.DTO;

public class QuizeAnswerDTO {

	private String id;
	private String question;
	private AnswerDetailsDTO answerDetails;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public AnswerDetailsDTO getAnswerDetails() {
		return answerDetails;
	}

	public void setAnswerDetails(AnswerDetailsDTO answerDetails) {
		this.answerDetails = answerDetails;
	}

}
