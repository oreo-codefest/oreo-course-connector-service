package com.ourse.connector.application.DTO;

import java.util.List;

import com.ourse.connector.application.Model.Course;

public class CourseResponseDTO {

	private String status;
	private List<Course> courseList;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public java.util.List<Course> getCourseList() {
		return courseList;
	}

	public void setCourseList(java.util.List<Course> courseList) {
		this.courseList = courseList;
	}

}
