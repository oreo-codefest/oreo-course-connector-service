package com.ourse.connector.application.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticationResponseDTO {

	private boolean active;
	private String role;

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getRole() {
		return role;
	}

//	public String getActive() {
//		return active;
//	}
//
//	public void setActive(String active) {
//		this.active = active;
//	}

	public void setRole(String role) {
		this.role = role;
	}

}
