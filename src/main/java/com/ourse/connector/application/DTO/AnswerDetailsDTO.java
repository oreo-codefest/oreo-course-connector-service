package com.ourse.connector.application.DTO;

public class AnswerDetailsDTO {

	private boolean answer;
	private String Description;

	public boolean isAnswer() {
		return answer;
	}

	public void setAnswer(boolean answer) {
		this.answer = answer;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}
}
