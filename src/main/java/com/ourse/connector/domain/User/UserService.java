package com.ourse.connector.domain.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ourse.connector.application.DTO.ResponseDTO;
import com.ourse.connector.application.Model.User;
import com.ourse.connector.application.transformer.Transformer;
import com.ourse.connector.infarstructure.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	Transformer transformer;

	@Autowired
	UserRepository userRepository;

	public ResponseDTO addUserToDB(User user) {

		try {
			userRepository.save(user);
			return transformer.createResponse("USER CREATED SUCCESSFULLY");

		} catch (Exception e) {
			return transformer.createResponse("USER CREATION FAILD");
		}

	}

	public Object getAllUsers() {

		try {
			
			List<User> userList = userRepository.findAll();
			return transformer.toUserResponseDTO(userList, "SUCCESS");

		} catch (Exception e) {
			return transformer.createResponse("MONGO EXCEPTION : USERS ARE NOT FOUND");
		}
	}

}
