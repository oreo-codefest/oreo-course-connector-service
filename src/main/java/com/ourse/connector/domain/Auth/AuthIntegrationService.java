package com.ourse.connector.domain.Auth;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ourse.connector.application.DTO.AuthAccessTokenResponseDTO;
import com.ourse.connector.application.DTO.AuthenticationResponseDTO;
import com.ourse.connector.infarstructure.ExternalProperties;

@Service
public class AuthIntegrationService {
	
	private Logger logger = LoggerFactory.getLogger(AuthIntegrationService.class);

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ExternalProperties externalProperties;

	public AuthenticationResponseDTO getAuthenticationResult(String authorization) throws JsonProcessingException {

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add("authorization", "Bearer " + externalProperties.getResourceServerToken());

		HttpEntity<String> request = new HttpEntity<>(headers);

		ResponseEntity<String> response = null;

		String url = externalProperties.getResourceServerUrl() + "token=" + authorization.replaceAll("Bearer ", "");

		try {

			logger.info("AUTH : IN PROGRESS : " + request);

			response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

			logger.info("RESPONSE : " + response);

		} catch (RestClientException e) {

			logger.info("AUTH : CRASHED");

			throw new RestClientException("AUTH DATA PROCCESSING EXCEPTION");

		}

		return (response != null) ? convertJSONToObject(response, AuthenticationResponseDTO.class) : null;
	}

	public AuthAccessTokenResponseDTO AuthRegister(String userEmail, String password) throws JsonProcessingException {

		logger.info("starter");

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();

		Map<String, String> bodyParamMap = new HashMap<String, String>();

		bodyParamMap.put("username", userEmail);
		bodyParamMap.put("password", password);

		String requestBoyData = new ObjectMapper().writeValueAsString(bodyParamMap);
		headers.add("authorization", "Bearer " + externalProperties.getResourceServerToken());

		HttpEntity<String> request = new HttpEntity<String>(requestBoyData, headers);

		ResponseEntity<String> response = null;

		String url = externalProperties.getResourceServerUrl();

		try {

			logger.info("AUTH DATA : IN PROGRESS");

			response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

			logger.info("RESPONSE : " + response);

		} catch (RestClientException e) {

			logger.info("AUTH DATA : CRASHED");

			throw new RestClientException("AUTH DATA PROCCESSING EXCEPTION");

		}

		return (response != null) ? convertJSONToObject(response, AuthAccessTokenResponseDTO.class) : null;
	}

	public String changeRole(String userEmail, String role) throws JsonProcessingException {

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();

		Map<String, String> bodyParamMap = new HashMap<String, String>();

		bodyParamMap.put("email", userEmail);
		bodyParamMap.put("newRole", role);

		String requestBoyData = new ObjectMapper().writeValueAsString(bodyParamMap);
		headers.add("authorization", "Bearer " + externalProperties.getResourceServerToken());

		HttpEntity<String> request = new HttpEntity<String>(requestBoyData, headers);

		ResponseEntity<String> response = null;

		String url = externalProperties.getResourceServerUrl();

		try {

			logger.info("AUTH DATA : IN PROGRESS");

			response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

			logger.info("RESPONSE : " + response);

			return new String("SUCCESS");

		} catch (RestClientException e) {

			logger.info("AUTH DATA : CRASHED");

			throw new RestClientException("AUTH DATA PROCCESSING EXCEPTION");

		}

	}

	public <T> T convertJSONToObject(ResponseEntity<String> response, Class<T> responseType) {

		T t = null;

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		try {

			t = objectMapper.readValue(response.getBody().toString(), responseType);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return t;
	}

	public <T> String convertObjectToJson(T t) {

		String output = null;
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		try {
			output = objectMapper.writeValueAsString(t);
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage(), e);
		}

		return output;
	}
}
