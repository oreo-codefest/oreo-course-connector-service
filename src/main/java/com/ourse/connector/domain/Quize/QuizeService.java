package com.ourse.connector.domain.Quize;

import com.ourse.connector.application.DTO.AssignQuizeRequestDTO;

public interface QuizeService {
	
	public Object addQuizeToDB(AssignQuizeRequestDTO quize);

}
