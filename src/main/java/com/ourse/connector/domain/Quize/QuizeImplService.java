package com.ourse.connector.domain.Quize;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ourse.connector.application.DTO.AssignQuizeRequestDTO;
import com.ourse.connector.application.Model.Course;
import com.ourse.connector.application.transformer.Transformer;
import com.ourse.connector.domain.Course.CourseServiceImpl;
import com.ourse.connector.infarstructure.repository.CourseRepository;
import com.ourse.connector.infarstructure.repository.QuizeRepository;

@Service
public class QuizeImplService implements QuizeService {

	private Logger logger = LoggerFactory.getLogger(CourseServiceImpl.class);

	@Autowired
	QuizeRepository quizeRepository;

	@Autowired
	Transformer transformer;

	@Autowired
	CourseRepository courseRepository;

	@Override
	public Object addQuizeToDB(AssignQuizeRequestDTO quize) {

		try {
			
			logger.info("QUIZE ADDING IN PROGRESS");

			Course course = courseRepository.findByCourseIdAndUnitTitle(quize.getId(), quize.getCourseTitle());

//			foreach(Course a : course.getUnit()){
//				
//			}

			courseRepository.save(course);

			return transformer.createResponse("QUIZE HAS BEEN ADDED TO THE COURSE UNIT");

		} catch (Exception e) {

			return transformer.createResponse("Exception : " + e);
		}

	}
}
