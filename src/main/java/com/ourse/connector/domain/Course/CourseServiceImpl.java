package com.ourse.connector.domain.Course;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ourse.connector.application.DTO.ResponseDTO;
import com.ourse.connector.application.Model.Course;
import com.ourse.connector.application.Model.Quize;
import com.ourse.connector.application.transformer.Transformer;
import com.ourse.connector.domain.Auth.AuthIntegrationService;
import com.ourse.connector.infarstructure.repository.CourseRepository;
import com.ourse.connector.infarstructure.repository.QuizeRepository;
import com.ourse.connector.utils.Constant;

@Service
public class CourseServiceImpl implements CourseService {

	private Logger logger = LoggerFactory.getLogger(CourseServiceImpl.class);

	@Autowired
	Transformer transformer;

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	QuizeRepository quizeRepository;

	@Override
	public ResponseDTO addCourseToDB(Course user) {

		try {

			logger.info("COURCE CREATION IN PROGRESS");

			courseRepository.save(user);
			return transformer.createResponse("COURSE CREATED SUCCESSFULLY");

		} catch (Exception e) {
			return transformer.createResponse("COURSE CREATION FAILD");
		}

	}

	@Override
	public ResponseDTO addQuizeToDB(Quize quize) {

		try {
			
			logger.info("IN PROGRESS");
			
			quizeRepository.save(quize);
			return transformer.createResponse("QUIZE ADDED SUCCESSFULLY");

		} catch (Exception e) {
			return transformer.createResponse("QUIZE CREATION FAILD");
		}

	}

	@Override
	public ResponseDTO updateInTheCourse(Course course) {

		try {

			Course updateCourse = new Course();
			updateCourse.setCourseQuiz(course.getCourseQuiz());
			courseRepository.save(updateCourse);

			return transformer.createResponse("COURSE UPDATED SUCCESSFULLY");

		} catch (Exception e) {

			return transformer.createResponse("COURSE UPDATE FAILD");
		}

	}

	@Override
	public Object getAllCourse() {
		try {

			List<Course> courseList = courseRepository.findAll();

			System.out.println(courseList);

			return transformer.toCourseResponseDTO(courseList, Constant.SUCCESS);

		} catch (Exception e) {

			return transformer.createResponse(Constant.FAILD + e);

		}

	}

	@Override
	public Object getCourseById(String courseId) {

		try {

			return courseRepository.findByCourseId(courseId);

		} catch (Exception e) {

			return transformer.createResponse(Constant.FAILD);

		}

	}

}
