package com.ourse.connector.domain.Course;

import com.ourse.connector.application.DTO.ResponseDTO;
import com.ourse.connector.application.Model.Course;
import com.ourse.connector.application.Model.Quize;

public interface CourseService {

	public ResponseDTO addCourseToDB(Course user);
	public ResponseDTO addQuizeToDB(Quize quize);
	public ResponseDTO updateInTheCourse(Course course);
	public Object getAllCourse(); 
	public Object getCourseById(String courseId);
	
}
