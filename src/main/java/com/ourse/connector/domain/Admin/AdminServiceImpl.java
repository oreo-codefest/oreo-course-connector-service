package com.ourse.connector.domain.Admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ourse.connector.application.DTO.AdminRoleRequestDTO;
import com.ourse.connector.application.transformer.Transformer;
import com.ourse.connector.domain.Auth.AuthIntegrationService;
import com.ourse.connector.infarstructure.repository.UserRepository;

@Service
public class AdminServiceImpl implements AdminService {

	private Logger logger = LoggerFactory.getLogger(AdminServiceImpl.class);

	@Autowired
	UserRepository userRepository;

	@Autowired
	Transformer transformer;

	@Autowired
	AuthIntegrationService authIntegrationService;

	@Override
	public Object chengeUserRoleAuthServer(AdminRoleRequestDTO adminRoleRequestDTO) throws JsonProcessingException {

		logger.info("CHANGING USER ROLE : PROCCESSING");

		return transformer.createResponse(
				authIntegrationService.changeRole(adminRoleRequestDTO.getEmail(), adminRoleRequestDTO.getRoles()));

	}

}
